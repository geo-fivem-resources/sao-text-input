# SAO Text Input

A Sword Art Online inspired text input system

**Usage:**
- `exports["sao-text-input"]:TextInput("Description", 50)`




**Parameters:**
- Description: string
- Max Length: int

**Preview:**
![alt text](https://gyazo.com/306dddfdb8272cd1445710fc4545eadb)
